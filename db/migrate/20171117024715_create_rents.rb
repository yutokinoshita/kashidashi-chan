class CreateRents < ActiveRecord::Migration
  def change
    create_table :rents do |t|
      t.references :user
      t.references :good

      t.datetime :expire_start_date
      t.datetime :expire_end_date
      t.datetime :return_date

      t.timestamps null: false
    end
  end
end
