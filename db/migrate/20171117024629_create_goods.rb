class CreateGoods < ActiveRecord::Migration
  def change
    create_table :goods do |t|
      t.string :goods_name, null: false, default: ""
      t.string :barcode, null: false, default: ""
      t.string :description, null: false, default: ""

      t.string :link

      t.references :user

      t.timestamps null: false
    end
  end
end
