# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(:user_name => '木下', :email => 'kinoshita@gmail.com', :password => '12345678', :password_confirmation => '12345678')
User.create(:user_name => '長崎', :email => 'nagasaki@gmail.com', :password => '12345678', :password_confirmation => '12345678')
User.create(:user_name => 'ヒェップ', :email => 'hiep@gmail.com', :password => '12345678', :password_confirmation => '12345678')

Good::create(:goods_name => 'ペンボックス', :barcode => '4903333172573', :description => '【水性なのに様々ものに書ける】発色がよく、にじみや裏写りのないマーカーです。耐水性に優れていて、水にも強い水性マーカーです。ポスターカラーのように鮮やかに発色し、素材を侵すことがなく、嫌なニオイもありません。乾くと重ね書きもでき、金属、ガラス、プラスチック、鏡など様々なものに書けます。', :user_id => 1 )
# Good::create(:goods_name => '教育体制', :barcode => '239084032233', :description => 'いい本ですね', :user_id => 2 )
# Good::create(:goods_name => '花火', :barcode => '4903333172573', :description => 'いい本ですね', :user_id => 2 )
# Good::create(:goods_name => 'Coaching Leadership', :barcode => '9784478008829', :description => 'いい本ですね', :user_id => 2 )

# Good::create(:goods_name => 'コーチングノウハウ', :barcode => '239084032233', :description => 'いい本ですね', :user_id => 2 )
# Good::create(:goods_name => '喧嘩稼業１巻', :barcode => '239084032234', :description => '面白い漫画！', :user_id => 2 )


Rent::create(:user_id => 1, :good_id => 1, :expire_start_date => '2017/11/01', :expire_end_date => '2017/11/16', :return_date => nil )
# Rent::create(:user_id => 2, :good_id => 2, :expire_start_date => '2017/11/01', :expire_end_date => '2017/11/14', :return_date => nil )
# Rent::create(:user_id => 1, :good_id => 2, :expire_start_date => '2017-11-17 04:58:50', :expire_end_date => '2017-11-19 04:58:50', :return_date => nil )

# Rent::create(:user_id => 2, :good_id => 1, :expire_start_date => '2017/11/15', :expire_end_date => '2017/11/29', :return_date => '2017/11/16' )

Favorite::create(:user_id => 2, :good_id => 1)
