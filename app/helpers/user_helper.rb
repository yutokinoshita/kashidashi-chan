module UserHelper
  def confirm_good_status(good_id)
    good_status_confirm = Rent.where(good_id: good_id, :return_date => nil).last
    if good_status_confirm.present?
       return "Someone use!"
     else
       return "You can rent NOW!"
    end
  end

  def username(user_id)
    name = User.find_by(id: user_id).user_name
    return name
  end

  def rent_user_name(good_id)
    rent = Rent.where(good_id: good_id).last
    if rent.present? && rent.user.present?
      rent.user.user_name
    else
      'なし'
    end
  end
end
