// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require plugins/jquery/jquery.min
//= require plugins/bootstrap/js/bootstrap
//= require plugins/bootstrap-select/js/bootstrap-select
//= require plugins/momentjs/moment
//= require plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker
//= require plugins/jquery-slimscroll/jquery.slimscroll
//= require plugins/node-waves/waves

//= require javascripts/admin
//= require javascripts/demo

// = require javascripts/quagga.min

//= require_tree .
