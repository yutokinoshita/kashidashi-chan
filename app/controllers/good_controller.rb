class GoodController < ApplicationController
  def index
    @goods = Good.search(params[:search])
  end

  def show
    @good = Good.find_by(id:params[:good_id])
    @rent = Rent.where(good_id: @good.id, :return_date => nil).last

    if @rent.present?
      @good_status = "貸し出し中です。リマインドしますか。";
      @good_status_params = 1
    else
      @good_status = "今借りる";
      @good_status_params = 2
    end
  end

  def ask
    good_status = params[:good_status]
    # good_status == 1 : remiind
    # good_status == 2 : rent soon
    if good_status == "1"
      if Favorite.find_by(user_id:current_user.id,good_id: params[:good_id]).present?
        flash[:notice] = "You already registered!"
      else
        remind = Favorite.create(user_id:current_user.id,good_id: params[:good_id])
          flash[:notice] = "Successed!Will remind you!"  if remind.valid?
      end

      redirect_to({:controller => 'user', :action=>'favorite' })
    else
      if Rent.find_by(user_id:current_user.id, good_id: params[:good_id]).present?
          flash[:notice] = "You rent already!"
      else
        if rent = Rent.create(user_id: current_user.id ,good_id: params[:good_id], expire_start_date: Time.now, expire_end_date:Time.now + 14.days )
          flash[:notice] = "Successed!You rented now!"
        end
      end
      redirect_to({:controller => 'user', :action=>'index' })
    end

  end

  def form
  end

  def search
  end
    helper_method :current_user

end
