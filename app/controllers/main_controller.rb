class MainController < ApplicationController
  def index

  end

  def camera
    @from = params[:from]
  end

  def rent
    if params[:good_id].present?
      good = Good.find(params[:good_id])

      expire_end_date = params[:return_date]

      if Rent.where(:user_id => current_user.id, :good_id => good.id, :return_date => nil ).present?
        @save_success = 1
      else
        @save_success = 2
        Rent.create(:user_id => current_user.id, :good_id => good.id, :expire_start_date => Time.now, :expire_end_date => expire_end_date)
      end
    else
      if params[:barcode].present?
        @barcode = params[:barcode]

        @good = Good.where(barcode: @barcode).first
      end
    end
  end

  def return
    if params[:barcode].present?
      barcode = params[:barcode]

      @good = Good.where(barcode: barcode).first

      if @good.present?
        @rents = Rent.where(:user_id => current_user.id, :good_id => @good.id)

        @rents.each do |rent|
          Rent.update(rent.id, :return_date => Time.now)
        end
      end
    end
  end

  def register
    if params[:barcode].present?
      barcode = params[:barcode]

      response = RestClient::Request.execute(
        method: :get,
        url: 'https://www.googleapis.com/books/v1/volumes?q=isbn:' + barcode
      )

      json = JSON.parse(response)

      if json["items"].present?
        @title = json["items"][0]["volumeInfo"]["title"]
        @description = json["items"][0]["volumeInfo"]["description"]
        @imglink = json["items"][0]["volumeInfo"]["imageLinks"]["thumbnail"]

        session[:title] = @title
        session[:barcode] = barcode
        session[:description] = @description
        session[:imglink] = @imglink
      end
    end
  end

  def register_save
    title = session[:title]
    barcode = session[:barcode]
    description = session[:description]
    imglink = session[:imglink]

    if Good.where(:barcode => barcode).present?
      @exist = true
    else
      Good::create(:goods_name => title, :barcode => barcode, :description => description, :user_id => current_user.id, :link => imglink)
      @save = true
    end
  end

end
