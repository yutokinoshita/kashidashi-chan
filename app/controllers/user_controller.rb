class UserController < ApplicationController
  def index
    @current_user ||= User.find_by(id: session[:id])
    @rents = Rent.where(user_id: current_user.id, :return_date => nil)
    @goods = Good.where(user_id: current_user.id)
  end

    def favorite
      @current_user ||= User.find_by(id: current_user.id)
      @favorites = Favorite.where(user_id: current_user.id)
    end
    helper_method :current_user
end
