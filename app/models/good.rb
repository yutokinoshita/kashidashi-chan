class Good < ActiveRecord::Base
  belongs_to :user

  def self.search(search) #self.でクラスメソッドとしている
    if search # Controllerから渡されたパラメータが!= nilの場合は、titleカラムを部分一致検索
      @goods = Good.where(['goods_name LIKE ?', "%#{search}%"])
    else
      @goods = Good.all #全て表示。
    end
  end
end
